package com.example.elmac.listageoreferenciada

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main_draw.*
import kotlinx.android.synthetic.main.app_bar_main_draw.*

class MainDrawActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_draw)
        setSupportActionBar(toolbar)


        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        nav_view.itemIconTintList to null

    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main_draw, menu)
        nav_view.itemIconTintList to null
        return true
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {

            R.id.nav_notificaciones -> {
                loadNotificacionesActivity(fragl = NotificacionesActivity())
            }
            R.id.nav_tareas -> {
                LoadTareasActivity(fragl = TareasActivity())
            }
            R.id.nav_perfil -> {
                loadPerfilActivity(fragl = PerfilActivity())
            }
            R.id.nav_grupos -> {
                loadGruposActivity(fragl = GruposActivity())
            }
            R.id.nav_listas -> {
                LoadListasActivity(fragl = ListasActivity())
            }
            R.id.nav_configuracion -> {
                loadConfiguracionesActivity(fragl = ConfiguracionesActivity())
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        nav_view.itemIconTintList to null
        return true
    }

    private fun LoadTareasActivity(fragl: TareasActivity) {
        val fm = supportFragmentManager.beginTransaction()
        fm.replace(R.id.frameLayout, fragl)
        fm.commit()
    }

    private fun LoadListasActivity(fragl: ListasActivity) {
        val fm = supportFragmentManager.beginTransaction()
        fm.replace(R.id.frameLayout, fragl)
        fm.commit()
    }

    private fun loadGruposActivity(fragl: GruposActivity) {
        val fm = supportFragmentManager.beginTransaction()
        fm.replace(R.id.frameLayout, fragl)
        fm.commit()
    }

    private fun loadPerfilActivity(fragl: PerfilActivity) {
        val fm = supportFragmentManager.beginTransaction()
        fm.replace(R.id.frameLayout, fragl)
        fm.commit()
    }

    private fun loadConfiguracionesActivity(fragl: ConfiguracionesActivity) {
        val fm = supportFragmentManager.beginTransaction()
        fm.replace(R.id.frameLayout, fragl)
        fm.commit()
    }

    private fun loadNotificacionesActivity(fragl: NotificacionesActivity) {
        val fm = supportFragmentManager.beginTransaction()
        fm.replace(R.id.frameLayout, fragl)
        fm.commit()
    }

}
