package com.example.elmac.listageoreferenciada


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button


class TareasActivity : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val banVerdana: Button = activity!!.findViewById(R.id.btnVerMapa)
        banVerdana.setOnClickListener {
            startActivity()
        }

        return LayoutInflater.from(container?.context).inflate(R.layout.activity_listas, container, false)

    }


    fun startActivity() {
        super.startActivity(Intent(activity, MapsActivity::class.java))
    }


}